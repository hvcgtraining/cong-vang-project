// Slick Slider
$(document).ready(function($) {
  $(".header-slider").slick({
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false
  });
});

// Responsive Navbar

function responsiveNavbar() {
  const navToggler = document.querySelector(".nav-toggler");
  const navBar = document.querySelector("ul.nav-menu");

  function showMenu(e) {
    navBar.classList.toggle("nav-active");
    navToggler.classList.toggle("toggle-icon");
  }

  navToggler.addEventListener("click", showMenu);
}

responsiveNavbar();

// Scroll Menu

window.onscroll = function() {
  scrollFunction();
};

function scrollFunction() {
  const headerMenu = document.querySelector(".header-menu");
  if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
    headerMenu.classList.add("scroll-menu");
  } else {
    headerMenu.classList.remove("scroll-menu");
  }
}
