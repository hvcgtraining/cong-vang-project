"use strict";

// Slick Slider
$(document).ready(function ($) {
  $(".header-slider").slick({
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false
  });
}); // Responsive Navbar

function responsiveNavbar() {
  var navToggler = document.querySelector(".nav-toggler");
  var navBar = document.querySelector("ul.nav-menu");

  function showMenu(e) {
    navBar.classList.toggle("nav-active");
    navToggler.classList.toggle("toggle-icon");
  }

  navToggler.addEventListener("click", showMenu);
}

responsiveNavbar(); // Scroll Menu

window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  var headerMenu = document.querySelector(".header-menu");

  if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {
    headerMenu.classList.add("scroll-menu");
  } else {
    headerMenu.classList.remove("scroll-menu");
  }
}